## Running QuakeJS webserver locally. 
Assuming You have Docker in Your system installed You need to run in Your terminal:
- Please replace YOUR_IP_ADDRESS with Your machine's internal IP address in Your local network via which other players will join the server. Executing command `ifconfig | grep "inet " | grep -v 127.0.0.1 | cut -d\  -f2` in terminal should return this address.
Then by executing `docker run -p 8080:80 -p 27960:27960 -e SERVER_ADDRESS='YOUR_IP_ADDRESS' registry.gitlab.com/elisauskas1/quakejs` You would run the server in Your PC in docker container.

Join the game at http://YOUR_IP_ADDRESS:8080 in Your browser or any PC browser in the same network.