FROM debian:buster

RUN apt update && apt upgrade \
    && apt install curl git nodejs npm apache2 jq wget -y \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && git clone --recurse-submodules https://github.com/begleysm/quakejs.git \
    && cd quakejs && npm install

COPY server.cfg /quakejs/base/baseq3/server.cfg
COPY server.cfg /quakejs/base/cpma/server.cfg
COPY ioq3ded.js /quakejs/build/ioq3ded.js
COPY start.sh /

RUN rm /var/www/html/index.html && cp /quakejs/html/* /var/www/html/ \
    && cd /var/www/html/ && bash /var/www/html/get_assets.sh \
    && echo "127.0.0.1 content.quakejs.com" >> /etc/hosts \
    && chmod +x /start.sh
COPY index.html /var/www/html/index.html

CMD ["bash","./start.sh"]